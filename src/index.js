const express = require('express');
const bodyParser = require("body-parser");
const app = express();

const PORT = 3080;

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * Routes.
 */
const usersRouter = require('./routes/users');

// routes
require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);

/**
 * Express middleware.
 */
// parses incoming requests with JSON payloads
app.use(express.json());
// parses incoming requests with urlencoded payloads
// extended: true - parsing the URL-encoded data with the querystring library
app.use(express.urlencoded({ extended: true }));

// app.use('/api', usersRouter);

function onStart() {
    console.log(`Server running on port ${PORT}`);
}

app.listen(PORT, onStart);

module.exports = app;