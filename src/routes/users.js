const router = require('express').Router();
const bcrypt = require("bcrypt");

const usersData = {
    1: { id: 1, firstName: 'Ralph', lastName: 'Woods' },
    2: { id: 2, firstName: 'Ronnie', lastName: 'Moore' },
    3: { id: 3, firstName: 'Koko', lastName: 'Kennedy' }
}

// signup route
router.route('/signup')
    // to create new resources
    .post(async (req, res, next) => {
        const body = req.body;
        if (!(body.email && body.password)) {
            return res.status(400).send({ error: "Data not formatted properly" });
        }
        // Return our request body and return status OK
        // generate salt to hash password
        const salt = await bcrypt.genSalt(10);
        // now we set user password to hashed password
        hashed = await bcrypt.hash(body.password, salt);
        res.json(hashed).status(200).send();
    })
    // to retrieve resource
    .get(function (req, res, next) {
        // Respond with some data and return status OK
        res.json(usersData);
        res.status(200).send();
    });

router.route('/users')
    // to create new resources
    .post(function (req, res, next) {
        // Return our request body and return status OK
        res.json(req.body).status(200).send();
    })
    // to retrieve resource
    .get(function (req, res, next) {
        // Respond with some data and return status OK
        res.json(usersData);
        res.status(200).send();
    });

router.route('/users/:userId').get(function (req, res, next) {
    const id = req.params.userId;
    if (id && usersData[id]) {
        res.json(usersData[id]);
        res.status(200).send();
    } else {
        // Not Found
        res.status(404).send();
    }
})
module.exports = router;

