const db = require("../models");
const config = require("../config/auth.config");
const User = db.User;
const Role = db.role;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  // Save User to Database
  User.create({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  })
    .then(user => {
      if (user) {
        res.send({ message: "User registered successfully!" });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};

exports.signin = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).send({ message: "User Not found." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Invalid Password!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });
      res.status(200).send({
        id: user.id,
        username: user.username,
        email: user.email,
        accessToken: token
      });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};


exports.viewAllProfile = (req, res) => {
  // View profile
  console.log(req.body)
  User.findAll({ 
    attributes: {exclude: ['password','createdAt','updatedAt']},
  })
    .then((users) => {
      if (!users) {
        res.send({ message: `There is no user` });
      }
      else{
        res.status(200).send(users);
      }

    }).catch((err) => {
      res.status(500).send({ message: err.message });
    })
};

exports.viewProfile = (req, res) => {
  // View profile
  console.log(req.body)
  User.findOne({ 
    where: { username: req.body.username },
    attributes: {exclude: ['password','createdAt','updatedAt']},
  })
    .then((user) => {
      if (!user) {
        res.send({ message: `There is no record of the username ${req.body.username}.` });
      }
      else{
        res.status(200).send(user);
      }

    }).catch((err) => {
      res.status(500).send({ message: err.message });
    })
};

exports.updateProfile = (req, res) => {
  // Update profile to Database
  User.update({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    dob: req.body.dob,
    phone: req.body.phone,
    email: req.body.email,
    building: req.body.building,
    street: req.body.street,
    city: req.body.city,
    state: req.body.state,
    postcode: req.body.postcode,
  },
    { where: { username: req.body.username } })
    .then(function (rowsUpdated) {
      res.send({ message: "Profile updated successfully!" });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};
