'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    let seed = [
      {
        first_name: 'James',
        last_name: 'Alan',
        username: 'james_alan',
        password: '$2b$10$e9qEMTeBa4T8LgdmwU8lnefExrzc0bTQRrYBN8IWaYXI6fdZ1rbcm',
        role: 'user',
        dob: '19/03/1992',
        phone: '60122456722',
        email: 'james_alan@gmail.com',
        building: 'No. 123, Jalan Emas',
        street: 'Sek. 230/92',
        city: 'Shah Alam',
        state: 'SEL',
        postcode: 40400,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        first_name: 'Rajesh',
        last_name: 'Menon',
        username: 'ramenon',
        password: '$2b$10$sInccAn21JgsRs8S9q1I6OdkuQBJGstJYv9G6fPn7Ly0vG25UZQ0S',
        role: 'user',
        dob: '19/03/1990',
        phone: '60123456722',
        email: 'rajeshmenon@gmail.com',
        building: 'No. 123, Jalan Itik',
        street: 'Sek. 230/9',
        city: 'Shah Alam',
        state: 'SEL',
        postcode: 40400,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        first_name: 'Mughis',
        last_name: 'Hussein',
        username: 'mughsein',
        password: '$2b$10$IXhArER1uQ40P1woj06kxeYEWJcTZSQMvrmSLzuIsbPq4RHCPquPy',
        role: 'user',
        dob: '19/03/1994',
        phone: '60123456789',
        email: 'mughishussein4@gmail.com',
        building: 'No. 123, Jalan Ayam',
        street: 'Sek. 300/9',
        city: 'Shah Alam',
        state: 'SEL',
        postcode: 40400,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]
    return queryInterface.bulkInsert('Users', seed, {});
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', [{
      username: 'mughsein'
    }])
  }
};
